#!/usr/bin/env node

const fs = require('fs');
const path = require('path');

const MARKDOWN_CONTENTS_DIRS = [
  'dist/content/nodes',
  'dist/content/marks',
  'dist/content',
];

const hugoHeaders = ['platform', 'product', 'category', 'subcategory'];
const hugoHeadersRegex = new RegExp(`^(${hugoHeaders.join('|')}):.+`, 'g');

function hugoHeadersReplacer(match, type) {
  switch (type) {
    case 'platform': return 'platform: stride';
    case 'product': return 'product: stride';
    case 'category': return 'category: reference';
    case 'subcategory': return 'subcategory: document';
  }
}

function processMarkdownFile(filePath) {
  let fileContents = fs.readFileSync(filePath, { encoding: 'utf-8' });

  // replace markdown links
  fileContents = fileContents.replace(/\[(.+?)\]\((\/platform\/document\-format.+?)\)/g, (match, linkTitle, linkURL) => {
    if (linkURL.startsWith('/platform/document-format/reference/nodes/')) {
      const url = linkURL.replace('/platform/document-format/reference/nodes/', '/cloud/stride/apis/document/nodes/');
      return `[${linkTitle}](${url})`;
    }

    if (linkURL.startsWith('/platform/document-format/reference/marks/')) {
      const url = linkURL.replace('/platform/document-format/reference/marks/', '/cloud/stride/apis/document/marks/');
      return `[${linkTitle}](${url})`;
    }

    if (linkURL.startsWith('/platform/document-format#')) {
      const anchor = linkURL.replace('/platform/document-format#', '');
      const url = `/cloud/stride/apis/document/structure#${anchor}`;

      return `[${linkTitle}](${url})`;
    }

    if (linkURL.endsWith('.png')) {
      const filePath = linkURL.replace('/platform', '');
      const imageFilePath = path.resolve(`${__dirname}/../content/${filePath}`);
      const base64 = fs.readFileSync(imageFilePath).toString('base64');
      const base64Uri = `data:image/png;base64,${base64}`;

      return `[${linkTitle}](${base64Uri})`;
    }

    throw new Error(`Unsupported markdown link: ${match}`);
  });

  // update hugo header
  const fileContentsLines = fileContents.split('\n');
  for (let i = 0; i < fileContentsLines.length; i++) {
    fileContentsLines[i] = fileContentsLines[i].replace(hugoHeadersRegex, hugoHeadersReplacer);
  }

  fileContents = fileContentsLines.join('\n');
  fs.writeFileSync(filePath, fileContents);
}

function extractStructure() {
  const items = [{
    title: 'Document structure',
    url: '/cloud/stride/apis/document/structure',
  }];

  const existingConfigFilePath = path.resolve(`${__dirname}/../data/document-format.json`);
  const existingConfigContents = fs.readFileSync(existingConfigFilePath);
  const existingConfig = JSON.parse(existingConfigContents);

  // get guides contents
  existingConfig.navigation.categories[0].subcategories[1].items.forEach(({ title, url }) => {
    const lastUrlChunk = url.split('/').pop();

    items.push({
      title,
      url: `/cloud/stride/apis/document/${lastUrlChunk}`,
    });
  });

  // get nodes
  existingConfig.navigation.categories[1].subcategories[0].items.forEach(({ title, url }) => {
    const lastUrlChunk = url.split('/').pop();

    items.push({
      title: `Node - ${title}`,
      url: `/cloud/stride/apis/document/nodes/${lastUrlChunk}`,
    });
  });

  // get marks
  existingConfig.navigation.categories[1].subcategories[1].items.forEach(({ title, url }) => {
    const lastUrlChunk = url.split('/').pop();

    items.push({
      title: `Mark - ${title}`,
      url: `/cloud/stride/apis/document/marks/${lastUrlChunk}`,
    });
  });

  const filePath = path.resolve(`${__dirname}/../dist/data.json`);
  const json = {
    title: 'Message Format',
    name: 'document',
    expandAlways: true,
    items,
  };

  fs.writeFileSync(filePath, JSON.stringify(json, null, 2));
}

function main() {
  for (let mdDirPath of MARKDOWN_CONTENTS_DIRS) {
    const dirPath = path.resolve(`${__dirname}/../${mdDirPath}`);
    const files = fs.readdirSync(dirPath);

    for (let fileName of files) {
      const filePath = path.resolve(`${dirPath}/${fileName}`);
      const stats = fs.statSync(filePath);

      if (!stats.isFile()) {
        continue;
      }

      processMarkdownFile(filePath);
    }
  }

  extractStructure();
}

main();
