---
title: Document builder
platform: platform
product: document-format
category: devguide
subcategory: misc
date: "2018-04-19"
---
# ADF builder

Insert some text in the WYSIWYG editor and see its Atlassian Document Format (ADF) representation below.

*(If nothing shows below, just wait a few seconds for the editor to load)*

<iframe src="https://s3-ap-southeast-2.amazonaws.com/atlaskit-storybooks/@atlaskit/editor-core/46.17.2/iframe.html?selectedKind=%40atlaskit%2Feditor-core&selectedStory=DAC%20editor%20example&full=0&down=1&left=1&panelRight=0&downPanel=kadirahq%2Fstorybook-addon-actions%2Factions-panel" style="border: none; width: 100%; height: 326px"></iframe>

<div class="highlight" style="background: #F4F5F7">
    <pre style="display: none; line-height: 125%" class="playground__json-output"></pre>
</div>

<script type="text/javascript">
var allowOrigins = ['https://s3-ap-southeast-2.amazonaws.com', 'http://localhost:9001'];
var code = document.querySelector('.playground__json-output');

addEventListener('message', function (evt) {
    if (allowOrigins.indexOf(evt.origin) === -1) {
        return;
    }

    if (!evt.data.editor) {
        return;
    }

    code.style.display = 'block';
    code.innerHTML = JSON.stringify(evt.data.doc, null, 2);
}, false);
</script>
