---
title: Client libraries
platform: platform
product: document-format
category: devguide
subcategory: misc
date: "2017-08-16"
---
# Client libraries

## Javascript

We currently support a client library for JavaScript: [adf-builder](https://www.npmjs.com/package/adf-builder)

This package offers two ways of building documents:

* A fluent document builder interface with support for all node types
* A tag to be used with ES6 template literals for single-paragraph documents

How to import it:

``` javascript
npm install adf-builder
```

For more detailed documentation, see [Atlassian Document Format Builder (JavaScript)](https://www.npmjs.com/package/adf-builder)

Example usage:

``` javascript
var { Document } = require('adf-builder');
const doc = new Document();
doc.paragraph()
    .text('Here is some ')
    .strong('bold test')
    .text(' and ')
    .em('text in italics')
    .text(' as well as ')
    .link(' a link', 'https://www.atlassian.com')
    .text(' , emojis ')
    .emoji(':smile:')
    .emoji(':rofl:')
    .emoji(':nerd:')
    .text(' and some code: ')
    .code('var i = 0;')
    .text(' and a bullet list');
doc.bulletList()
    .textItem('With one bullet point')
    .textItem('And another');
doc.panel("info")
    .paragraph()
    .text("and an info panel with some text, with some more code below");
doc.codeBlock("javascript")
    .text('var i = 0;\nwhile(true) {\n  i++;\n}');
var reply = doc.toJSON();
```

## Python

You can get the library using pip or any Python package manager that can use PyPi packages: `pip install pyadf`. The library currently only supports Python 3+.

You can construct your document nodes manually if you want, but the recommended way of constructing documents is to use the fluent API.

```python
from pyadf.document import Document

doc = Document()                           \
    .paragraph()                           \
        .emoji("success")                  \
        .text(' This is my document, ')    \
        .text('this is a link')            \
            .link('https://atlassian.com') \
    .end()                                 \
    .paragraph()                           \
        .text('... and another paragraph') \
    .to_doc()
```
