---
title: Node - heading
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-11-21"
---

# Node - heading

## Purpose

The `heading` node represents a heading (e.g. &lt;h1&gt;) and contains only inline nodes.

## Example

```json
{
  "type": "heading",
  "attrs": {
    "level": 1
  },
  "content": [
    {
      "type": "text",
      "text": "Heading 1"
    }
  ]
}
```

## Content

`heading` is a block node in the [top-level blocks](/platform/document-format#top-level-blocks-group-nodes) group that contains only inline nodes as children.

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"heading"||
|content||array|Array of zero or more inline nodes||
|attrs|✔|object|||
|attrs.level|✔|integer|1-6|The heading level (following the same convention as HTML), e.g. 1 → &lt;h1&gt;>|
