---
title: Node - codeBlock
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-11-28"
---

# Node - codeBlock

## Purpose

The `codeBlock` node contains text containing code.

## Example

```json
{
  "type": "codeBlock",
  "attrs": {
    "language": "javascript"
  },
  "content": [
    {
      "type": "text",
      "text": "var foo = {};\nvar bar = [];"
    }
  ]
}
```

## Content

`codeBlock` is a block node in the [top-level blocks](/platform/document-format#top-level-blocks-group-nodes) group that can only contain text. It cannot contain other children.

## Languages

Any string is accepted as a code language. Currenly we use [Prism](https://prismjs.com/) for syntax highlighting. You can view the languages that Prism supports [here](https://github.com/conorhastings/react-syntax-highlighter/blob/master/AVAILABLE_LANGUAGES_PRISM.MD).

If the language is `text` or is unsupported it will render as plain monospaced text.

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"codeBlock"||
|content||array|"codeBlock"||
|attrs||object|||
|attrs.language||string||If the language is [supported](#languages) it will be highlighted, otherwise will render as plain monospaced text|
