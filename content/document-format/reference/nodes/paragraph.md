---
title: Node - paragraph
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-11-21"
---
# Node - paragraph

## Purpose

The `paragraph` node is a container for a sequence of inline nodes.

## Example

```json
{
  "type": "paragraph",
  "content": [
    {
      "type": "text",
      "text": "Hello world"
    }
  ]
}
```

## Content

`paragraph` is a block node in the [top-level blocks](/platform/document-format#top-level-blocks-group-nodes) group that contains [inlines group nodes](/platform/document-format#inlines-group-nodes) as children.

## Fields

|Name   |Mandatory|Type  |Value      |Notes|
|-------|---------|------|-----------|-----|
|type|✔|string|"paragraph"||
|content||array|Array of 0 or more inline nodes||
