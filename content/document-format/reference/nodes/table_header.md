---
title: Node - tableHeader
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-04-05"
---

# Node - tableHeader

{{% warning %}}
Please note that this is currently only supported on **web** and **desktop**.  
**Mobile** rendering support for tables is not available yet.
{{% /warning %}}

## Content

Array of one or more of these nodes:

- [Panel](/platform/document-format/reference/nodes/panel)
- [Paragraph](/platform/document-format/reference/nodes/paragraph)
- [Blockquote](/platform/document-format/reference/nodes/blockquote)
- [OrderedList](/platform/document-format/reference/nodes/orderedList)
- [BulletList](/platform/document-format/reference/nodes/bulletList)
- [Rule](/platform/document-format/reference/nodes/rule)
- [Heading](/platform/document-format/reference/nodes/heading)
- [CodeBlock](/platform/document-format/reference/nodes/codeBlock)
- [MediaGroup](/platform/document-format/reference/nodes/mediaGroup)
- [ApplicationCard](/platform/document-format/reference/nodes/applicationCard)
- [DecisionList](/platform/document-format/reference/nodes/decisionList)
- [TaskList](/platform/document-format/reference/nodes/taskList)
- [Extension](/platform/document-format/reference/nodes/extension)
- [NestedExpand](/platform/document-format/reference/nodes/nestedExpand)

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"tableHeader"||
|attrs||object|||
|attrs.background||string|Short or long hex color, or [HTML color name](http://dev.w3.org/csswg/css-color/#named-colors).||
|attrs.colspan||integer|Positive integer of column span count.|Defaults to 1.|
|attrs.colwidth||array|Array of integers that describe the (possibly merged) column widths, in pixels.|The length of the array should be equal to the number of spanned columns. 0 is permitted as an *array value* if the column size is not fixed (e.g. an cell merged across 3 columns -- one unfixed surrounded by two fixed -- might be represented as `[120, 0, 120]`).|
|attrs.rowspan||integer|Positive integer of row span count.|Defaults to 1.|
|content|✔|array|Array of one or more **top-level blocks** nodes.|Can support any **top-level blocks** node children.|
