---
title: Node - media
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-05-15"
---

# Node - media

## Purpose

The `media` node represents a single file or link stored in media services.

## Examples

```json
{
  "type": "media",
  "attrs": {
    "type" : "file",
    "id": "6e7c7f2c-dd7a-499c-bceb-6f32bfbf30b5",
    "collection" : "my project files"
  }
}
```

## Content

`media` is an immutable node that can not be modified.

## Marks

`media` does not support any marks.

## Siblings / parents / children

`media` is a node that can only occur within the following nodes:

 * [mediaGroup](/platform/document-format/reference/nodes/mediaGroup)
 * [mediaSingle](/platform/document-format/reference/nodes/mediaSingle)

## Notes and recommendations

 * Media id (represented as attrs.id) is immutable in Media API.
 * Consumers of the document should always fetch fresh metadata, for media with given ID, from Media API
## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"media"||
|attrs|✔|object|||
|attrs.width||integer||Field will not be shown if not provied within mediaSingle|
|attrs.height||integer||Field will not be shown if not provied within mediaSingle|
|attrs.id|✔|string|Media services ID|Used for querying media services API to retrieve metadata (e.g. filename)|
|attrs.type|✔|string|"file"||
|attrs.collection|✔|string|Media Services Collection name||
|attrs.occurrenceKey||string|Non empty string|An occurrence key needs to be present in order to enable deletion of files from a collection|
