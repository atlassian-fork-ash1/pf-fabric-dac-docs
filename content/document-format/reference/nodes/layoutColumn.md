---
title: Node - layoutColumn
platform: platform
product: document-format
category: reference
subcategory: nodes
date: "2019-06-12"
---

# Node - layoutColumn

## Purpose

Represents a column of content inside a [layoutSection](/platform/document-format/reference/nodes/layoutSection).

## Content

Can contain anything you'd normally have at the top level of a document, except another [layoutSection](/platform/document-format/reference/nodes/layoutSection).

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|content|✔|array|Array of one or more [top-level nodes](/platform/document-format#top-level-blocks-group-nodes) nodes **except** [layoutSection](/platform/document-format/reference/nodes/layoutSection).||
|type|✔|string|"layoutColumn"||
|attrs|✔|object|||
|attrs.width|✔|number|Floating point number between 0 and 100.|Percentage width relative to its parent `layoutSection`<br /><br />Usually rounded to 2 decimal places.|