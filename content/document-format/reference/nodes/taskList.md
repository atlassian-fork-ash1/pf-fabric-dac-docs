---
title: Node - taskList
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-03-08"
---

# Node - taskList

## Purpose

The `taskList` node is a container for [taskItem](/platform/document-format/reference/nodes/taskItem) or [taskList](/platform/document-format/reference/nodes/taskList) nodes.

## Example

```json
{
  "type": "taskList",
  "attrs": {
    "localId": "test-list-id"
  },
  "content": [
    {
      "type": "taskItem",
      "attrs": {
        "localId": "test-id",
        "state": "TODO"
      },
      "content": [
        {
          "type": "text",
          "text": "Do this!"
        }
      ]
    },
    {
      "type": "taskItem",
      "attrs": {
        "localId": "test-id",
        "state": "DONE"
      },
      "content": [
        {
          "type": "text",
          "text": "This is completed"
        }
      ]
    }
  ]
}
```

## Content

`taskList` is a block node in the [top-level blocks](/platform/document-format#top-level-blocks-group-nodes) group that contains [taskItem](/platform/document-format/reference/nodes/taskItem) or [taskList](/platform/document-format/reference/nodes/taskList) nodes as children, but a taskItem must come first.

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"taskList"||
|content|✔|array|Array of [taskItem](/platform/document-format/reference/nodes/taskItem) or [taskList](/platform/document-format/reference/nodes/taskList) nodes ||
|attrs|✔|object|||
|attrs.localId|✔|string|||
