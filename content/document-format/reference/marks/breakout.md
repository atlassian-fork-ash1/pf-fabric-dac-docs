---
title: Mark - breakout
platform: none
product: document-format
category: reference
subcategory: marks
date: "2019-06-18"
---

# Mark - breakout

## Purpose

The `breakout` mark is intended to be applied to different block nodes, e.g. [codeBlock](/platform/document-format/reference/nodes/codeBlock) to switch the display mode of a node that makes it expands wider than the content area.

## Example

```json
{
  "type": "codeBlock",
  "content": [],
  "marks": [
    {
      "type": "breakout",
      "attrs": {
        "mode": "wide"
      }
    }
  ]
}
```

## Fields

| Name       | Mandatory | Type   | Value                  |
| ---------- | --------- | ------ | ---------------------- |
| type       | ✔         | string | "breakout"             |
| attrs      | ✔         | object |                        |
| attrs.mode | ✔         | string | "wide" or "full-width" |
