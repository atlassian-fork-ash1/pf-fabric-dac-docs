---
title: Mark - action
platform: platform
product: document-format
category: reference
subcategory: marks
date: "2018-03-14"
---

# action

# example

```json
      "type": "text",
      "text": "Open Dialog",
      "marks": [{
        "type": "action",
        "attrs": {
          "key": "unique-action-mark-key",
          "title": "view dialog",
          "target": {
            "key": "app-dialog"
          },
          "parameters": {
            "reportId": "123"
          }
        }
      }]
```

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"action"|||
|attrs|✔|object|||
|attrs.target|✔|object|||
|attrs.target.key|✔|string|||
|attrs.target.receiver||string|||
|attrs.title|✔|string|||
|attrs.key||string||Unique key for action|
