# Atlassian Document Format developer documentation

This repository contains documentation for 3rd party developers about Atlassian Document Format. Documentation is written in Markdown.

## What's inside

Inside this directory you will find the content and navigation structure for your docs. These will
be published at:

https://developer.atlassian.com/cloud/stride/apis/document/structure/

```
README.md
node_modules/
package.json
.gitignore
.spelling
content/
document-format/
    reference/
        nodes/
        marks/
        index.md
    index.md
    playground.md
data/
    document-format.json
```

## Preview your documentation set locally

You can instantly preview changes to your documentation set as you make them using the `npm run preview` command. See the [viewing docs locally guide](https://developer.atlassian.com/dac/viewing-docs-locally/) for full details.

## Spell check with npm test

Once the installation is done, you can run the `npm test` command inside the project folder:

* `npm test`: Runs a spell checker on Markdown files
* `npm run-script spellcheck`: Interactively fix or ignore spelling errors

Edit the `.spelling` file to add words to your dictionary.

## Release your documentation set

Edit the files, then create pull request. When the PR is merged, DAC will consume all the changes from the repo. Usually DAC is being upgraded daily.
